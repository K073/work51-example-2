import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React</h1>
                    <ul className="App-nav">
                        <li><a href="" className="App-link">home</a></li>
                        <li><a href="" className="App-link">portfolio</a></li>
                        <li><a href="" className="App-link">about</a></li>
                        <li><a href="" className="App-link">services</a></li>
                        <li><a href="" className="App-link">contact</a></li>
                    </ul>
                </header>
            </div>
        );
    }
}

export default App;
