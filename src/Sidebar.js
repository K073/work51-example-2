import React, { Component } from 'react';
import './Sidebar.css';

class Sidebar extends Component {
    render () {
        return (
            <div className="Sidebar">
                <ul className="Sidebar-nav">
                    <li><a href="" className="Sidebar-link">item</a></li>
                    <li><a href="" className="Sidebar-link">item</a></li>
                    <li><a href="" className="Sidebar-link">item</a></li>
                    <li><a href="" className="Sidebar-link">item</a></li>
                    <li><a href="" className="Sidebar-link">item</a></li>
                </ul>
            </div>
        );
    }
}

export default Sidebar;