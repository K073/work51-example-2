import React, { Component } from 'react';
import './Content.css';

class Content extends Component{
    render() {
        return (
            <div className="Content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium corporis debitis deserunt ducimus error in magnam nesciunt nisi obcaecati quaerat, quia quibusdam quod rerum saepe sed tempora tenetur, veniam voluptas!</p>
                <img src="http://lorempixel.com/output/animals-q-g-640-480-5.jpg" alt=""/>
            </div>
        );
    }
}

export default Content;