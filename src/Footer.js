import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component{
    render() {
        return (
            <div className="Footer">
                <p className="Footer-copy">© K073 Hello react.js</p>
            </div>
        );
    }
}

export default Footer;