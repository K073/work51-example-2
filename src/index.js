import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Sidebar from "./Sidebar";
import Content from "./Content";
import Footer from "./Footer";

let Wrap = (
    <div className="Wrap">
        <App/>
        <div className="Container">
            <Sidebar/>
            <Content/>
        </div>
        <Footer/>
    </div>
);

ReactDOM.render(Wrap, document.getElementById('root'));
registerServiceWorker();
